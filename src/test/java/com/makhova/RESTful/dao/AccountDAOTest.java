package com.makhova.RESTful.dao;

import com.makhova.RESTful.model.Account;
import com.makhova.RESTful.utils.AccountException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import static java.math.BigDecimal.ROUND_HALF_UP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AccountDAOTest {
    private Field field = null;
    private AccountDAO accountDAO;

    @Before
    public void setUp() throws Exception {
        accountDAO = AccountDAO.getInstance();
        ConcurrentHashMap<Integer, Account> accountMap = new ConcurrentHashMap<>();
        Account account1 = new Account(1, "Тестовый", new BigDecimal(5000.00).setScale(2, ROUND_HALF_UP));
        Account account2 = new Account(2, "Anna", new BigDecimal(500.00).setScale(2, ROUND_HALF_UP));
        Account account3 = new Account(3, "Andrew", new BigDecimal(0).setScale(2, ROUND_HALF_UP));
        accountMap.put(account1.getId(), account1);
        accountMap.put(account2.getId(), account2);
        accountMap.put(account3.getId(), account3);
        field = AccountDAO.class.getDeclaredField("accountMap");
        field.setAccessible(true);
        field.set(null, accountMap);
    }

    @After
    public void tearDown() throws Exception {
        field = AccountDAO.class.getDeclaredField("instance");
        field.setAccessible(true);
        field.set(null, null);
    }

    @Test
    public void transact() throws Exception {
        Account account1 = new Account(1, "Тестовый", new BigDecimal(4700.00).setScale(2, ROUND_HALF_UP));
        Account account2 = new Account(2, "Anna", new BigDecimal(800.00).setScale(2, ROUND_HALF_UP));
        List<Account> expected = new ArrayList<>();
        expected.add(account1);
        expected.add(account2);
        List<Account> actual = accountDAO.transact(1, 2, new BigDecimal(300));
        assertEquals(expected, actual);
    }

    @Test(expected = AccountException.class)
    public void transactAccountException() throws AccountException {
        accountDAO.transact(3, 1, new BigDecimal(300));
    }

    @Test(expected = NullPointerException.class)
    public void transactNullPointerException() throws AccountException {
        accountDAO.transact(4, 1, new BigDecimal(300));
    }

    @Test
    public void getAccount() throws Exception {
        Account expected = new Account(1, "Тестовый", new BigDecimal(5000).setScale(2, ROUND_HALF_UP));
        Account actual = accountDAO.getAccount(1);
        assertEquals(expected, actual);
        actual = accountDAO.getAccount(6);
        assertNull(actual);
    }

    @Test
    public void getAllAccounts() throws Exception {
        Account account1 = new Account(1, "Тестовый", new BigDecimal(5000.00).setScale(2, ROUND_HALF_UP));
        Account account2 = new Account(2, "Anna", new BigDecimal(500.00).setScale(2, ROUND_HALF_UP));
        Account account3 = new Account(3, "Andrew", new BigDecimal(0).setScale(2, ROUND_HALF_UP));
        List<Account> expected = new ArrayList<>();
        expected.add(account1);
        expected.add(account2);
        expected.add(account3);
        List<Account> actual = accountDAO.getAllAccounts();
        assertEquals(expected, actual);
    }
}