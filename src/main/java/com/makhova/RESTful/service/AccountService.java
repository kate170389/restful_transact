package com.makhova.RESTful.service;

import com.makhova.RESTful.utils.AccountException;
import com.makhova.RESTful.dao.AccountDAO;
import com.makhova.RESTful.model.Account;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.ROUND_HALF_UP;

@Path("/accounts")
public class AccountService {
    private AccountDAO accountDAO = AccountDAO.getInstance();

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Account> getAllAccount() {
        return accountDAO.getAllAccounts();
    }

    @PUT
    @Path("/{accountIdFrom}/{accountIdTo}/{sum}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Account> getAccount(@PathParam("accountIdFrom") Integer accountIdFrom,
                                    @PathParam("accountIdTo") Integer accountIdTo,
                                    @PathParam("sum") BigDecimal sum) throws AccountException {
        if (accountDAO.getAccount(accountIdFrom) != null && accountDAO.getAccount(accountIdTo) != null) {
            return accountDAO.transact(accountIdFrom, accountIdTo, sum.setScale(2, ROUND_HALF_UP));
        } else {
            throw new AccountException("Account not found");
        }
    }
}