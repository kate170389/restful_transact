package com.makhova.RESTful.dao;

import com.makhova.RESTful.model.Account;
import com.makhova.RESTful.utils.AccountException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class AccountDAO {
    private static volatile AccountDAO instance;
    private static ConcurrentHashMap<Integer, Account> accountMap;
    private static AtomicInteger countId;

    private AccountDAO() {
        accountMap = new ConcurrentHashMap<>();
        countId = new AtomicInteger(0);
        Account account1 = new Account(countId.addAndGet(1), "Tom", new BigDecimal(65000.90).setScale(2, ROUND_HALF_UP));
        Account account2 = new Account(countId.addAndGet(1), "Anna", new BigDecimal(760.00).setScale(2, ROUND_HALF_UP));
        Account account3 = new Account(countId.addAndGet(1), "Andrew", new BigDecimal(198675.56).setScale(2, ROUND_HALF_UP));
        accountMap.put(account1.getId(), account1);
        accountMap.put(account2.getId(), account2);
        accountMap.put(account3.getId(), account3);
    }

    public static AccountDAO getInstance() {
        if (instance == null) {
            synchronized (AccountDAO.class) {
                if (instance == null) {
                    instance = new AccountDAO();
                }
            }
        }
        return instance;
    }

    public List<Account> transact(int accountIdFrom, int accountIdTo, BigDecimal sum) throws AccountException {
        List<Account> accounts = new ArrayList<>();
        synchronized (AccountDAO.class) {
            Account accountFrom = accountMap.get(accountIdFrom);
            Account accountTo = accountMap.get(accountIdTo);
            BigDecimal moneyAccountFrom = accountFrom.getMoney();
            BigDecimal moneyAccountTo = accountTo.getMoney();
            if (moneyAccountFrom.compareTo(sum) > 0) {
                accountFrom.setMoney(moneyAccountFrom.subtract(sum).setScale(2, ROUND_HALF_UP));
                accountTo.setMoney(moneyAccountTo.add(sum).setScale(2, ROUND_HALF_UP));
                accounts.add(accountFrom);
                accounts.add(accountTo);
            } else {
                throw new AccountException("Account " + accountIdFrom + " haven't money.");
            }
        }
        return accounts;
    }

    public Account getAccount(Integer accountId) {
        return accountMap.get(accountId);
    }

    public List<Account> getAllAccounts() {
        List<Account> list = new ArrayList<Account>();
        list.addAll(accountMap.values());
        return list;
    }
}